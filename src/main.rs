use std::fs;

use anyhow::{bail, Context, Result};
use log::{debug, info, warn};
use reqwest::Client;

use crate::graphql::ResultError;

mod authn;
mod config;
mod graphql;

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init();
    debug!("Reading configuration");
    let config = config::read_settings().context("Reading configuration failed")?;
    debug!("Building HTTP client");
    let client = Client::new();
    debug!("Getting access token");
    let mut access_token = authn::get_access_token(
        &client,
        &config.token_endpoint,
        &config.client_id,
        &config.client_secret,
    )
    .await
    .with_context(|| {
        format!(
            "Getting access token from {} failed",
            &config.token_endpoint
        )
    })?;

    fs::create_dir_all("dumps")?;
    let mut docs_counter = 0;
    let mut bulk_size = config.bulk_size;
    if let Some(ids) = config.memobase_ids {
        for id in ids {
            loop {
                let iri = format!("https://data.connectome.ch/memobase/dataset/{}", &id);
                let variables = graphql::fetch::Variables {
                    iri,
                    extended: config.extended,
                };
                let graphql_res = graphql::fetch_query(
                    &client,
                    &config.graphql_endpoint,
                    &access_token.token_value,
                    variables,
                )
                .await
                .with_context(|| {
                    format!("Getting datasets from {} failed", &config.graphql_endpoint)
                })?;
                if let Some(data) = graphql_res.data {
                    docs_counter = graphql::dump_single_document(
                        &data,
                        &format!("dumps/{id}.jsonl"),
                        docs_counter,
                    )?;
                };
                info!("{} documents processed by now", docs_counter);
                if let Some(err) = graphql_res.errors {
                    if authn::not_authenticated(&err) {
                        warn!("Client not authenticated! Getting new access token");
                        access_token = authn::get_access_token(
                            &client,
                            &config.token_endpoint,
                            &config.client_id,
                            &config.client_secret,
                        )
                        .await
                        .with_context(|| {
                            format!(
                                "Getting access token from {} failed",
                                &config.token_endpoint
                            )
                        })?;
                        continue;
                    }
                    err.into_iter()
                        .map(|e| {
                            let ge: ResultError = e.into();
                            serde_json::to_string(&ge).expect("Serialisation of error failed")
                        })
                        .for_each(|e| warn!("{}", &e));
                };
                break;
            }
        }
    } else {
        let mut res: Option<graphql::search_dataset::ResponseData> = None;
        let mut max_docs: Option<usize> = None;
        let mut bulk_counter = 0;
        while let Some(c) = graphql::get_cursor(res.as_ref()) {
            loop {
                let mut items_downloaded = false;
                if config.max_docs.filter(|max| *max <= docs_counter).is_some() {
                    info!(
                        "Maximum amount of documents ({}) reached. Finishing",
                        config.max_docs.unwrap()
                    );
                    return Ok(());
                }
                bulk_size = if let Some(md) = max_docs {
                    if md < docs_counter + bulk_size as usize {
                        u16::try_from(md - docs_counter)?
                    } else {
                        bulk_size
                    }
                } else {
                    bulk_size
                };
                debug!("Getting {} objects with cursor {}", bulk_size, c);
                let variables = graphql::get_variables(&c, bulk_size, config.extended);
                let graphql_res = graphql::search_dataset_query(
                    &client,
                    &config.graphql_endpoint,
                    &access_token.token_value,
                    variables,
                )
                .await
                .with_context(|| {
                    format!("Getting datasets from {} failed", &config.graphql_endpoint)
                })?;
                if let Some(data) = graphql_res.data {
                    if bulk_counter == 0 {
                        max_docs = config
                            .max_docs
                            .or(Some(usize::try_from(data.search_dataset.total_count)?));
                    }
                    bulk_counter += 1;
                    docs_counter = graphql::dump_data(
                        &data,
                        &format!("dumps/{bulk_counter:04}.jsonl"),
                        docs_counter,
                        config.max_docs,
                    )?;
                    res = Some(data);
                    items_downloaded = true;
                };
                info!("{} documents processed by now", docs_counter);
                if let Some(err) = graphql_res.errors {
                    warn!("Got {} errors in response", err.len());
                    if authn::not_authenticated(&err) {
                        warn!("Client not authenticated! Getting new access token");
                        access_token = authn::get_access_token(
                            &client,
                            &config.token_endpoint,
                            &config.client_id,
                            &config.client_secret,
                        )
                        .await
                        .with_context(|| {
                            format!(
                                "Getting access token from {} failed",
                                &config.token_endpoint
                            )
                        })?;
                        continue;
                    }
                    err.into_iter()
                        .map(|e| {
                            let ge: ResultError = e.into();
                            serde_json::to_string(&ge).expect("Serialisation of error failed")
                        })
                        .for_each(|e| warn!("{}", &e));
                };
                if !items_downloaded {
                    bail!("Download ended with errors.")
                }
                break;
            }
        }
    }
    info!("All documents ({}) downloaded", docs_counter);
    Ok(())
}
