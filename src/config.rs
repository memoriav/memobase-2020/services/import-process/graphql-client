use std::{
    fs::File,
    io::{BufReader, Read},
};

use anyhow::Context;
use clap::Parser;
use http::Uri;
use log::info;
use serde::Deserialize;

#[derive(Deserialize, Default, Parser)]
#[serde(rename_all = "camelCase", deny_unknown_fields)]
#[command(author, version, about, long_about = None)]
#[clap(rename_all = "kebab-case")]
struct InputConfig {
    /// GraphQL endpoint URI
    #[serde(default)]
    #[arg(short, long, env, verbatim_doc_comment)]
    pub graphql_endpoint: Option<String>,
    /// Access token endpoint URI
    #[serde(default)]
    #[arg(short, long, env, verbatim_doc_comment)]
    pub token_endpoint: Option<String>,
    /// Client id (required for getting access token)
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub client_id: Option<String>,
    /// Client secret (required for getting access token)
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub client_secret: Option<String>,
    /// Maximal amount of documents to be fetched
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub max_docs: Option<usize>,
    /// Bulk download size limit. Defaults to 100 (the allowed maximum)
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub bulk_size: Option<u16>,
    /// Fetch extended metadata
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment)]
    pub extended: bool,
    /// Memobase document ID for fetching individual documents
    #[serde(default)]
    #[arg(long, env, verbatim_doc_comment, value_delimiter = ',')]
    pub memobase_ids: Option<Vec<String>>,
    /// Path to configuration file
    #[serde(skip_deserializing)]
    #[arg(short = 'c', long, env)]
    pub config_file: Option<String>,
}

pub struct Config {
    /// GraphQL endpoint URI
    pub graphql_endpoint: Uri,
    /// Access token endpoint URI
    pub token_endpoint: Uri,
    /// Client id (required for getting access token)
    pub client_id: String,
    /// Client secret (required for getting access token)
    pub client_secret: String,
    /// Maximal amount of documents to be fetched
    pub max_docs: Option<usize>,
    /// Bulk download size limit
    pub bulk_size: u16,
    /// Fetch extended metadata
    pub extended: bool,
    /// Memobase document ID for fetching individual documents
    pub memobase_ids: Option<Vec<String>>,
}

impl Config {
    pub(self) fn build(cli_config: InputConfig, file_config: InputConfig) -> anyhow::Result<Self> {
        Ok(Self {
            graphql_endpoint: cli_config
                .graphql_endpoint
                .or(file_config.graphql_endpoint)
                .and_then(|ge| ge.parse::<Uri>().ok())
                .context("setting graphql_endpoint required")?,
            token_endpoint: cli_config
                .token_endpoint
                .or(file_config.token_endpoint)
                .and_then(|te| te.parse::<Uri>().ok())
                .context("setting token_endpoint required")?,
            client_id: cli_config
                .client_id
                .or(file_config.client_id)
                .context("setting client_id required")?,
            client_secret: cli_config
                .client_secret
                .or(file_config.client_secret)
                .context("setting client_id required")?,
            max_docs: cli_config.max_docs.or(file_config.max_docs),
            bulk_size: cli_config
                .bulk_size
                .or(file_config.bulk_size)
                .unwrap_or(100),
            memobase_ids: cli_config.memobase_ids.or(file_config.memobase_ids),
            extended: cli_config.extended || file_config.extended,
        })
    }
}

/// Creates a [`Config`] instance from the different possible input sources
pub fn read_settings() -> anyhow::Result<Config> {
    let cli_config = InputConfig::parse();
    let path = cli_config
        .config_file
        .clone()
        .unwrap_or("configs/main.toml".to_string());
    let file_config = if let Ok(file) = File::open(&path) {
        let mut reader = BufReader::new(file);
        let mut buf = String::new();
        reader.read_to_string(&mut buf)?;
        toml::from_str(&buf)?
    } else {
        info!("No config file found under {}", &path);
        InputConfig::default()
    };
    Config::build(cli_config, file_config)
}
