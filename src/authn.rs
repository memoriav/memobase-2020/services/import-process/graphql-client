use graphql_client::Error;
use std::collections::HashMap;

use anyhow::{bail, Context, Result};
use http::Uri;
use reqwest::Client;
use serde::{Deserialize, Deserializer};
use serde_json::Value;

/// Represents an access token returned by respective endpoint
#[derive(Deserialize)]
pub struct AccessToken {
    #[serde(rename = "access_token")]
    pub token_value: String,
    pub expires_in: usize,
    pub refresh_expires_in: usize,
    pub token_type: String,
    #[serde(rename = "not-before-policy")]
    pub not_before_policy: usize,
    #[serde(deserialize_with = "string_tokenizer")]
    pub scope: Vec<String>,
}

/// Return vector of strings by deserializing and splitting string at whitespaces
fn string_tokenizer<'de, D>(deserializer: D) -> Result<Vec<String>, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(deserializer)?;
    Ok(s.split(' ').map(ToString::to_string).collect())
}

/// Get access token from respective endpoint
pub async fn get_access_token(
    client: &Client,
    endpoint_uri: &Uri,
    client_id: &str,
    client_secret: &str,
) -> Result<AccessToken> {
    let mut form_body = HashMap::new();
    form_body.insert("grant_type", "client_credentials");
    form_body.insert("client_id", client_id);
    form_body.insert("client_secret", client_secret);
    let response = client
        .post(&endpoint_uri.to_string())
        //.header("Content-Type", "application/x-www-form-urlencoded")
        .form(&form_body)
        .send()
        .await?;
    if !response.status().is_success() {
        bail!(
            "Getting access token failed. HTTP code {} returned by server",
            response.status().as_u16()
        );
    }
    let json: AccessToken = response
        .json()
        .await
        .context("Parsing response body as access token failed")?;
    Ok(json)
}

/// Looks for hints in error message if client is not authenticated
pub fn not_authenticated(err: &[Error]) -> bool {
    err.first()
        .and_then(|e| e.extensions.as_ref())
        .and_then(|ext| ext.get("originalError"))
        .and_then(Value::as_object)
        .and_then(|oe| oe.get("statusCode"))
        .and_then(Value::as_u64)
        .filter(|status| *status == 403)
        .is_some()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_access_token_parsing() {
        let access_token = r#"{"access_token":"abcd","expires_in":86400,"refresh_expires_in":0,"token_type":"Bearer","not-before-policy":0,"scope":"email odn-b2b silver profile"}"#;
        let res: Result<AccessToken, serde_json::Error> = serde_json::from_str(access_token);
        assert!(res.is_ok());
        assert!(res.unwrap().scope.contains(&"silver".to_string()));
    }
}
