use std::{fs::File, io::Write};

use anyhow::Result;
use graphql_client::{GraphQLQuery, QueryBody, Response};
use http::Uri;
use log::debug;
use reqwest::Client;
use serde::Serialize;

/// Type alias for custom scalar type `DateTime`
type DateTime = String;

/// Search dataset query
#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "schemas/connectome_schema.graphql",
    query_path = "schemas/search_dataset_query.graphql",
    response_derives = "Debug, Serialize",
    skip_serializing_none
)]
pub struct SearchDataset;

/// Fetches a single Dataset
#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "schemas/connectome_schema.graphql",
    query_path = "schemas/fetch_query.graphql",
    response_derives = "Debug, Serialize",
    skip_serializing_none
)]
pub struct Fetch;

#[derive(Serialize)]
pub struct ResultError {
    pub message: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub locations: Option<Vec<Location>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub path: Option<Vec<String>>,
}

impl From<graphql_client::Error> for ResultError {
    fn from(value: graphql_client::Error) -> Self {
        let message = value.message;
        let locations = value.locations.map(|vec| {
            vec.into_iter()
                .map(|loc| Location {
                    line: loc.line,
                    column: loc.column,
                })
                .collect::<Vec<Location>>()
        });
        let path = value.path.map(|vec| {
            vec.into_iter()
                .map(|pf| pf.to_string())
                .collect::<Vec<String>>()
        });
        Self {
            message,
            locations,
            path,
        }
    }
}

#[derive(Serialize)]
pub struct Location {
    pub line: i32,
    pub column: i32,
}

/// Build and send dataset query to endpoint
pub async fn search_dataset_query(
    client: &Client,
    graphql_endpoint: &Uri,
    access_token: &str,
    variables: search_dataset::Variables,
) -> Result<graphql_client::Response<search_dataset::ResponseData>> {
    let query_body: QueryBody<search_dataset::Variables> = SearchDataset::build_query(variables);
    let res = client
        .post(&graphql_endpoint.to_string())
        .header("Authorization", format!("Bearer {access_token}"))
        .json(&query_body)
        .send()
        .await?;
    let res_body: Response<search_dataset::ResponseData> = res.json().await?;
    Ok(res_body)
}

/// Buld and set fetch query to endpoint
pub async fn fetch_query(
    client: &Client,
    graphql_endpoint: &Uri,
    access_token: &str,
    variables: fetch::Variables,
) -> Result<graphql_client::Response<fetch::ResponseData>> {
    let query_body: QueryBody<fetch::Variables> = Fetch::build_query(variables);
    let res = client
        .post(&graphql_endpoint.to_string())
        .header("Authorization", format!("Bearer {access_token}"))
        .json(&query_body)
        .send()
        .await?;
    let res_body: Response<fetch::ResponseData> = res.json().await?;
    Ok(res_body)
}

/// Create variables for upcoming request
pub fn get_variables(cursor: &str, bulk_size: u16, extended: bool) -> search_dataset::Variables {
    search_dataset::Variables {
        // workaround for https://github.com/graphql-rust/graphql-client/issues/439
        cursor: Some(cursor.to_string()),
        first: Some(i64::from(bulk_size)),
        provider: Some(search_dataset::Provider::memobase),
        query: "*".to_string(),
        extended,
    }
}

/// Extract cursor for next bulk from response data
pub fn get_cursor(response_data: Option<&search_dataset::ResponseData>) -> Option<String> {
    if response_data.is_none() {
        return Some(String::new());
    }
    if let Some(ref edges) = response_data.unwrap().search_dataset.edges {
        if let Some(elem) = edges.iter().last() {
            return Some(elem.cursor.clone());
        }
    }
    None
}

/// Write dataset to file
pub fn dump_data(
    response_data: &search_dataset::ResponseData,
    file_name: &str,
    mut docs_counter: usize,
    max_docs: Option<usize>,
) -> Result<usize> {
    if let Some(ref edges) = response_data.search_dataset.edges {
        debug!("Got {} data items", edges.len());
        if !edges.is_empty() {
            let mut file = File::create(file_name)?;
            for elem in edges {
                if max_docs.filter(|max| *max <= docs_counter).is_some() {
                    return Ok(docs_counter);
                }
                writeln!(file, "{}", &serde_json::to_string(&elem.node)?)?;
                docs_counter += 1;
            }
        }
    } else {
        debug!("No edges found");
    }
    Ok(docs_counter)
}

/// Write single retrieved document to file
pub fn dump_single_document(
    response_data: &fetch::ResponseData,
    file_name: &str,
    docs_counter: usize,
) -> Result<usize> {
    let mut file = File::create(file_name)?;
    writeln!(file, "{}", &serde_json::to_string(&response_data.fetch)?)?;
    Ok(docs_counter + 1)
}
