FROM scratch
ENTRYPOINT ["/app/app"]
WORKDIR /app
COPY target/x86_64-unknown-linux-musl/release/graphql-client app
